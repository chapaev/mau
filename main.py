#!/usr/bin/python3
# =============================================================================
# dependency.py
# =============================================================================


class Dependency(object):
    def __init__(self):
        self.group_id = None
        self.artifact_id = None
        self.version = None
        self.group_id_pos = None
        self.artifact_id_pos = None
        self.version_pos = None
        self.dependencies = None
        self.path = None
        self.file = None


# =============================================================================
# printer.py
# =============================================================================


class ArtifactTreePrinter():
    def __init__(self, version=False, sep =' -> '):
        self._print_version = version
        self._sep = sep

    def print(self, artifacts_tree, iter=0):
        args = []

        args.append(artifacts_tree[0].artifact_id)

        if self._print_version:
            args.append(artifacts_tree[0].version)

        self._print_leaf(self._sep.join(map(str, args)), iter)
        if artifacts_tree[1]:
            for art in artifacts_tree[1]:
                self.print(art, iter + 1)

    def _print_leaf(self, artifact_id, iter):
        print('|  ' * iter, '+- ', artifact_id, sep='')


class ArtifactVersionPrinter():
    '''Отображает информацию об артефакте'''

    def __init__(self, path=False, sep=' -> '):
        self._print_path = path
        self._sep = sep

    def print(self, pom):
        args = []

        args.append(pom.version)

        if self._print_path:
            args.append(pom.path)

        print(self._sep.join(map(str, args)))


class ArtifactVersionPrinterExtended():
    '''Отображает информацию об артефакте'''

    def __init__(self, path=False, sep='.'):
        self._print_path = path
        self._sep = sep

    def print(self, pom):
        args = []

        args.append(pom.group_id)
        args.append(pom.artifact_id)
        #args.append(pom.version)

        if self._print_path:
            args.append(pom.path)

        print(self._sep.join(map(str, args)))


class ArtifactUsePrinter():
    '''Отображает информацию о списке зависимостей'''

    def __init__(self, group=False, artifact=True, path=False, pom_ver=False, dep_artifact=False, sep=' -> '):
        self._print_group = group
        self._print_artifact = artifact
        self._print_path = path
        self._sep = sep
        self._pom_ver = pom_ver
        self._dep_artifact = dep_artifact

    def print(self, pom, dep):
        args = []

        args.append(dep.version)

        if self._dep_artifact:
            args.append(dep.artifact_id)

        if self._print_group:
            args.append(pom.group_id)
        if self._print_artifact:
            args.append(pom.artifact_id)
        if self._pom_ver:
            args.append(pom.version)
        if self._print_path:
            args.append(pom.path)

        print(self._sep.join(map(str, args)))


# =============================================================================
# artifactBuilder.py
# =============================================================================


class TagDepth(object):
    def __init__(self):
        self.tag_depth = []

    def get_path(self):
        val = ''
        for tag in self.tag_depth:
            val += (tag + '|')
        return val

    def rem(self):
        if len(self.tag_depth) > 0:
            del self.tag_depth[-1]

    def add(self, tag_name):
        self.tag_depth.append(tag_name)


def get_xml_info(file_name):
    main = build_structure(file_name)
    fill_data(main)
    if main.dependencies:
        filter_dep(main)
    return main


def build_structure(file_name):
    file_arr = read_file_to_arr(file_name)

    tag_found = False
    tag_name = c = ''

    main = Dependency()
    main.file = file_arr
    dependencies = []
    main.dependencies = dependencies

    tag_depth = TagDepth()

    row = 0
    found_comment = False
    while row < len(file_arr):
        line = file_arr[row]
        col = 0
        while col < len(line):

            pc, c = c, line[col]
            col += 1

            if c == '\n':
                break
            elif c == '<':
                tag_found = True
                tag_name = ''
                continue
            elif c == '!' and pc == '<':
                tag_found = False
                found_comment = True
                continue
            elif c == '>' and pc == '/':
                tag_found = False
                tag_name = ''
                continue
            elif c == '>' and pc == '-' and found_comment:
                found_comment = False
                continue

            if found_comment:
                continue

            if tag_found:
                if c in ['/','?'] and pc == '<':
                    tag_found = False
                    tag_depth.rem()
                    continue
                elif c == '>' or c == ' ':
                    tag_found = False
                    if len(tag_name) > 0:
                        path = tag_depth.get_path()
                        if path == 'project|dependencies|':
                            dependencies.append(Dependency())
                        elif path == 'project|':
                            fill_pos(main, tag_name, row, col)
                        elif path == 'project|dependencies|dependency|':
                            dep = dependencies[-1]
                            fill_pos(dep, tag_name, row, col)

                        tag_depth.add(tag_name)
                    continue
                elif c != ' ':
                    tag_name += c
                else:
                    tag_found = False
        row += 1
    return main


def fill_pos(dep, tag_name, row, col):
    if tag_name == 'groupId':
        dep.group_id_pos = (row, col)
    elif tag_name == 'artifactId':
        dep.artifact_id_pos = (row, col)
    elif tag_name == 'version':
        dep.version_pos = (row, col)

def filter_dep(art):
    for dep in art.dependencies:
        if package_mask not in dep.group_id:
            art.dependencies.remove(dep)

def fill_data(dep_info):
    fil_dep(dep_info.file, dep_info)

    for dep in dep_info.dependencies:
        fil_dep(dep_info.file, dep)


def fil_dep(arr, dep):
    row, col = dep.group_id_pos
    dep.group_id = extract(arr[row], col)

    row, col = dep.artifact_id_pos
    dep.artifact_id = extract(arr[row], col)

    if dep.version_pos:
        row, col = dep.version_pos
        dep.version = extract(arr[row], col)


def extract(str, pos):
    return str[pos:str.find('<', pos)]


# =============================================================================
# ioutils.py
# =============================================================================


import os

class PomFileFinder(object):
    def find(self):
        result = []
        for root, dirs, files in os.walk('.'):
            for file in files:
                if file == "pom.xml" and path_mask in root:
                    result.append(os.path.join(root, file).replace("\\","/"))
        return result


def read_file_to_arr(file_name):
    with open(file_name, 'r', encoding='utf-8') as file:
        file_arr = file.readlines()
    return file_arr


import codecs
def wtite_to_file(art):
    with codecs.open(art.path, 'w', 'utf-8') as file:
        for line in art.file:
            file.write(line)


# =============================================================================
# logic.py
# =============================================================================


def get_artifacts(files):
    '''
    Получение списка артефактов-объектов на основании списка файлов
    :param files: список файлов
    :return: список объектов-артефактов
    '''
    result = []
    for file in files:
        arcifact = get_xml_info(file)
        arcifact.path = file
        if package_mask in arcifact.group_id:
            result.append(arcifact)
    return result


def get_artifact(art_name, arts):
    '''
    Поиск артефакта по его имени
    :param art_name: имя
    :param arts: список объектов-артефактов
    :return: артефакт
    '''
    for art in arts:
        if art.artifact_id == art_name:
            return art
    return None


def get_artifact_use(art_name, arts):
    '''
    Поиск в зависимостях конкретного артефакта
    :param art_name: имя артефакта-зависимости
    :param arts: список объектов-артефактов
    :return: список артефактов, которые используют art_name
    '''
    result = []
    for art in arts:
        found = get_artifact(art_name, art.dependencies)
        if found:
            result.append(art)
    return result


def get_artifacts_vs_dep_snapshots(arts):
    result = []
    for art in arts:
        for dep in art.dependencies:
            if dep.version and 'SNAPSHOT' in dep.version:
                result.append(art)
                break
    return result

    
def get_artifact_vs_dep_snapshots(arts, cur_art):
    result = []
    for art in arts:
        if cur_art.artifact_id == art.artifact_id:
            return get_artifacts_vs_dep_snapshots([art])
    return result    

def get_snapshot_deps(art):
    result = []
    for dep in art.dependencies:
        if dep.version and 'SNAPSHOT' in dep.version:
            result.append(dep)
    return result
    
def get_artifacts_final(arts):
    result = []
    for art in arts:
        final = True
        for art_ in arts:
            for dep in art_.dependencies:
                if art.artifact_id == dep.artifact_id:
                    final = False
                    break
        if final:
            result.append(art)
    return result


def get_artifacts_tree(art_name, arts):
    '''Построение дерева артефактов от текущего'''
    artifact = get_artifact(art_name, arts)
    if not artifact:
        return None

    dependencies = get_artifact_use(art_name, arts)
    if not dependencies:
        return (artifact, None)

    dependency_lst = []
    for dependency in dependencies:
        dependency_lst.append(get_artifacts_tree(dependency.artifact_id, arts))
    return (artifact, dependency_lst)


def get_root_artifacts(arts):
    '''Поиск корневых зависимостей'''
    result = []
    for art in arts:
        dependencies = get_artifact_use(art.artifact_id, arts)
        if not dependencies:
            continue
        
        if not art.dependencies:
            result.append(art)
            continue
        
        is_root = True
        for dep in art.dependencies:
            artifact = get_artifact(dep.artifact_id, arts)
            if artifact:
                is_root = False
                break
        if is_root:
            result.append(art)
    
    return result

def upd_artifact_use_ver(artifact, ver, arts):
    for art in arts:
        for dep in art.dependencies:
            if dep.artifact_id == artifact:
                row, col = dep.version_pos
                update(art, row, col, dep.version, ver)
                dep.version = ver
                wtite_to_file(art)

def upd_artifact(art, ver):
    row, col = art.version_pos
    update(art, row, col, art.version, ver)
    wtite_to_file(art)

def upd_to_snap_rec(artifact, arts, rec=False):
    cur_artifact = get_artifact(artifact, arts)
    if not artifact:
        return None

    cur_used = get_artifact_use(cur_artifact.artifact_id, arts)
    if len(cur_used) == 0:
        return None

    upd_artifact_use_ver(cur_artifact.artifact_id, cur_artifact.version, cur_used)

    if rec:
        for cur_art in cur_used:
            upd_to_snap_rec(cur_art.artifact_id, arts, rec)

def upd_to_last_rel(artifact, arts, rec=False):
    cur_artifact = get_artifact(artifact, arts)
    if not artifact:
        return None

    cur_used = get_artifact_use(cur_artifact.artifact_id, arts)
    if len(cur_used) == 0:
        return None

    ver = extract_release_ver(cur_artifact.version)

    upd_artifact_use_ver(cur_artifact.artifact_id, ver, cur_used)            

def extract_release_ver(ver):
    ver = ver.split(".")
    minor = ver[len(ver) - 1]
    minor = minor.replace("-SNAPSHOT", "")
    ver[len(ver) - 1] = str(int(minor) - 1)
    return ".".join(ver)
    


def update(art, row, col, old, new):
    line = art.file[row]
    art.file[row] = replace(line, col, old, new)


def replace(text, pos, old, new):
    return text[:pos] + str(new) + text[pos+len(old):]


# =============================================================================
# commandLineArgs.py
# =============================================================================


class Command(object):
    def __init__(self, command, art=None, ver=None):
        self.command = command
        self.art = art
        self.ver = ver


def parse_command(args):
    if len(args) >= 2 and args[1] == '-snap':
        return Command('snap')
    elif len(args) >= 2 and args[1] == '-roots':
        return Command('roots', args[1])        
    elif len(args) >= 2 and args[1] == '-rtree':
        return Command('rtree', args[1])
    elif len(args) >= 2 and args[1] == '-releasetree':
        return Command('releasetree', args[1])        
    if len(args) >= 2 and args[1] == '-final':
        return Command('final')        
    elif len(args) >= 3 and args[1] == '-ver':
        return Command('ver', args[2])
    elif len(args) >= 3 and args[1] == '-vere':
        return Command('vere', args[2])
    elif len(args) >= 3 and args[1] == '-rc':
        return Command('rc', args[2])        
    elif len(args) >= 3 and args[1] == '-use':
        return Command('use', args[2])
    elif len(args) >= 3 and args[1] == '-upds':
        return Command('upds', args[2])
    elif len(args) >= 3 and args[1] == '-updr':
        return Command('updr', args[2])
    elif len(args) >= 3 and args[1] == '-updsr':
        return Command('updsr', args[2])        
    elif len(args) >= 3 and args[1] == '-tree':
        return Command('tree', args[2])
    elif len(args) >= 4 and args[1] == '-upd':
        return Command('upd', args[2], args[3])
    elif len(args) >= 4 and args[1] == '-updm':
        return Command('updm', args[2], args[3])
    else:
        print_help()
        exit()


# =============================================================================
# helper.py
# =============================================================================


def print_help():
    help = """
    Утилита для работы с множеством проектов java, использующих сборщик maven.
    Позволяет получать информацию о текущем проекте - его версия, кто его использует, дерево зависимостей.
    Позволяет обновлять версию артефакта у всех, кто её использует, на необходимую.

    Формат запуска:
        main.py -{0} {1} {2} 

    {0} - ver|use|tree|snap|upd|final|rc|roots|upds|updsr|rtree|updr|releasetree
    {1} - имя артефакта
    {2} - версия

    -snap
        поиск всех проектов, у которых есть зависимости SHAPSHOT
        main.py -snap

    -roots
        поиск всех корневых зависимостей
        main.py -roots
        
    -final
        поиск всех конечных проектов 
        main.py -final        

    -ver
        поиск артефакта
        main.py -ver serv1

    -vere
        поиск артефакта
        main.py -vere serv1

    -rc
        определяет, готов ли артефакт для релиза, по наличию SNAPSHOT в зависимостях
        main.py -rc serv1         

    -use
        поиск проектов, использующих этот артефакт
        main.py -use serv1

    -tree
        построение дерева зависимостей от этого артефакта
        main.py -tree serv1

    -rtree
        построение дерева зависимостей от всех корневых библиотек
        main.py -rtree    

    -releasetree     
        построение дерева зависимостей от первых изменённых библиотек (по наличию их SNAPSHOT зависимостей у кого-то)
        main.py -rtree    

    -upd serv1 0.1
        обновление версии артефакта у всех, кто его использует
        main.py -upd serv1 0.1 

    -updm serv1 0.1
        обновление версии артефакта
        main.py -upd serv1 0.1

    -upds serv1
        обновление версии артефакта у всех, кто его использует, на текущую в разработке
        main.py -upds serv1

    -updr serv1
        обновление версии артефакта у всех, кто его использует, на релиз (убрать SNAPSHOT)
        main.py -updr serv1        

    -updsr serv1
        обновление версии артефакта у всех, кто его использует, на текущую в разработке (рекурсивно)
        main.py -updsr serv1        
    """
    print(help)


# =============================================================================
# runner.py
# =============================================================================


def run(args):
    command = parse_command(args)

    files = PomFileFinder().find()
    if len(files) == 0:
        print("poms not found")
        exit()

    artifacts = get_artifacts(files)

    if command.command == 'ver':
        art = get_artifact(command.art, artifacts)
        if not (art):
            print("art not found")
            exit()
        ArtifactVersionPrinter(path=True).print(art)

    if command.command == 'vere':
        art = get_artifact(command.art, artifacts)
        if not (art):
            print("art not found")
            exit()
        ArtifactVersionPrinterExtended(path=False).print(art)        

    elif command.command == 'use':
        arts = get_artifact_use(command.art, artifacts)
        if len(arts) == 0:
            print("arts not found")
            exit()
        printer = ArtifactUsePrinter(path=True, artifact=False, dep_artifact=False)
        for art in arts:
            dep_art = get_artifact(command.art, art.dependencies)
            printer.print(art, dep_art)

    elif command.command == 'tree':
        arts = get_artifacts_tree(command.art, artifacts)
        if not (arts) or len(arts) == 0:
            print("arts not found")
            exit()
        ArtifactTreePrinter(version=True).print(arts)

    elif command.command == 'rtree':
        arts = get_root_artifacts(artifacts)
        for art in arts:
            cur_tree = get_artifacts_tree(art.artifact_id, artifacts)
            if cur_tree and len(cur_tree) > 0:
                ArtifactTreePrinter(version=True).print(cur_tree)
                print()        

    elif command.command == 'releasetree':
        arts = get_root_artifacts(artifacts)
        print("to be continued...")                     

    elif command.command == 'roots':
        arts = get_root_artifacts(artifacts)
        for art in arts:
            ArtifactVersionPrinter(path=True).print(art)
         

    elif command.command == 'snap':
        arts = get_artifacts_vs_dep_snapshots(artifacts)
        if len(arts) == 0:
            print("arts not found")
            exit()
        printer = ArtifactUsePrinter(artifact=False, path=True, dep_artifact=True)
        for art in arts:
            for dep in get_snapshot_deps(art):
                printer.print(art, dep)
                
    elif command.command == 'rc':
        art = get_artifact(command.art, artifacts)
        if not (art):
            print("art not found")
            exit()
        
        arts = get_artifact_vs_dep_snapshots(artifacts, art)
        if not arts:
            print("art is ready for release")
            exit()
            
        printer = ArtifactUsePrinter(artifact=False, path=False, dep_artifact=True)
        for art in arts:
            for dep in get_snapshot_deps(art):
                printer.print(art, dep)
                
    elif command.command == 'final':
        arts = get_artifacts_final(artifacts)
        if len(arts) == 0:
            print("arts not found")
            exit()
        printer = ArtifactVersionPrinter(path=True)
        for art in arts:
            printer.print(art)
                
    elif command.command == 'upd':
        arts = get_artifact_use(command.art, artifacts)
        if len(arts) == 0:
            print("arts not found")
            exit()
        upd_artifact_use_ver(command.art, command.ver, arts)

    elif command.command == 'updm':
        art = get_artifact(command.art, artifacts)
        if not (art):
            print("art not found")
            exit()
        upd_artifact(art, command.ver)

    elif command.command == 'upds':
        art = get_artifact(command.art, artifacts)
        if not art:
            print("arts not found")
            exit()
        upd_to_snap_rec(command.art, artifacts, False)

    elif command.command == 'updsr':
        art = get_artifact(command.art, artifacts)
        if not art:
            print("arts not found")
            exit()
        upd_to_snap_rec(command.art, artifacts, True)        
    
    elif command.command == 'updr':
        art = get_artifact(command.art, artifacts)
        if not art:
            print("arts not found")
            exit()
        upd_to_last_rel(command.art, artifacts, False)


# =============================================================================
# config.py
# =============================================================================

#маска для пути поиска файлов
path_mask = ''

#маска для поиска пакетов проектов
package_mask = ''

# =============================================================================
# run.py
# =============================================================================

import sys

if __name__ == "__main__":
    # args = [0,1,2,3]
    # args[1] = '-upd'
    # args[2] = 'serv1'
    # args[3] = '11'
    # run(args)
    run(sys.argv)
